package com.efaceless.web.repository;

import com.efaceless.web.entity.Emotion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmotionRepository extends CrudRepository<Emotion, Long> {

    @Query("select e from Emotion e order by e.creationDateTime desc")
    List<Emotion> findAll();

    @Query("select e.id, e.desc from Emotion e")
    List<String> findAllDescs();

    @Query("select distinct e.type from Emotion e")
    List<String> findAllTypes();
}
