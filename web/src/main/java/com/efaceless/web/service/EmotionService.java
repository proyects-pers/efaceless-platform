package com.efaceless.web.service;

import com.efaceless.web.entity.Emotion;

import java.util.List;

public interface EmotionService {

    public List<Emotion> retrieveEmotions();

    List<String> retrieveEmotionDescs();

    void save(Emotion emotion);

    List<String> retrieveEmotionTypes();
}
